+++
author = "Alban"
title = "Docker setup for Hugo static site generator"
date = "2020-06-26"
description = "A post that talk about how to set up Hugo using Docker"
tags = [
    "hugo",
    "docker",
]
categories = [
    "hugo",
    "docker",
]
series = ["Hugo setup and deploy"]
+++
This article describes the basic setup for Hugo using docker
<!--more-->

Let`s have a look at the Docker file setup

We use alpine linux. We set up Hugo version environment variables.

{{< highlight bash "linenos=table" >}}
FROM alpine:3.12

# config
ENV HUGO_VERSION=0.72.0
ENV HUGO_TYPE=_extended
ENV HUGO_ID=hugo${HUGO_TYPE}_${HUGO_VERSION}
{{< / highlight >}}

Then we update or upgrade the alpine install some required tools.
{{< highlight bash "linenos=table" >}}
RUN apk update \
    && apk upgrade \
    && apk add --no-cache git asciidoctor libc6-compat libstdc++ bash \
    && rm -rf /var/cache/apk/*
{{< / highlight >}}

We copy the entrypoint script, we download hugo, and we install it under `/usr/local/sbin/hugo` so it is available globally and in the end we change the entrypoint permission. 
{{< highlight bash "linenos=table" >}}
COPY ./build/entrypoint.sh /tmp/entrypoint.sh

RUN wget -O - https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_ID}_Linux-64bit.tar.gz | tar -xz -C /tmp \
    && mkdir -p /usr/local/sbin \
    && mv /tmp/hugo /usr/local/sbin/hugo \
    && rm -rf /tmp/${HUGO_ID}_linux_amd64 \
    && rm -rf /tmp/LICENSE.md \
    && rm -rf /tmp/README.md \
    && chmod 0777 /tmp/entrypoint.sh
{{< / highlight >}}

In the end we set up volumes, working directory, port, and the entry point. At this point the `Dockerfile` is ready.
{{< highlight bash "linenos=table" >}}
VOLUME /site
VOLUME /public
WORKDIR /site

ENTRYPOINT ["/tmp/entrypoint.sh"]

EXPOSE 1313
{{< / highlight >}}

The entry point script.

{{< highlight bash "linenos=table" >}}
#!/bin/sh

set -ex

HUGO=/usr/local/sbin/hugo
WATCH="${HUGO_WATCH:=false}"
SLEEP="${HUGO_REFRESH_TIME:=-1}"
HUGO_DEV_DESTINATION="${HUGO_DEV_DESTINATION:=/public}"
HUGO_PRD_DESTINATION="${HUGO_PRD_DESTINATION:=/published}"
echo "HUGO_WATCH:" $WATCH
echo "HUGO_REFRESH_TIME:" $HUGO_REFRESH_TIME
echo "HUGO_DEV_BASEURL" $HUGO_DEV_BASEURL
echo "HUGO_PRD_BASEURL" $HUGO_PRD_BASEURL
echo "ARGS" $@
echo "Hugo path: $HUGO"

while [ -z "$@" ]
do
    if [[ $HUGO_WATCH != 'false' ]]; then
	    echo "Watching..."
        $HUGO server --watch=true --source="/site" --destination="$HUGO_DEV_DESTINATION" --baseURL="$HUGO_DEV_BASEURL" --bind="0.0.0.0" "$@" || exit 1
    fi

    if [[ $HUGO_REFRESH_TIME == -1 ]]; then
        exit 0
    fi
    echo "Sleeping for $HUGO_REFRESH_TIME seconds..."
    sleep $SLEEP
done

if [[ $@ == 'compile' ]]; then
    echo "compile theme"
    $HUGO --source="/site" --destination="$HUGO_PRD_DESTINATION" --baseURL="$HUGO_PRD_BASEURL" || exit 1
    exit 0
fi

exec $@
{{< / highlight >}}

for local development we want Hugo to run server with `--watch=true` so it monitors for file changes and compiles them on the fly

The basic .env file content.
{{< highlight bash "linenos=table" >}}
HUGO_WATCH=true
HUGO_DEV_DESTINATION=/public
HUGO_PRD_DESTINATION=/public
HUGO_DEV_BASEURL=http://0.0.0.0
HUGO_PRD_BASEURL=https://unsettled.dev/
{{< / highlight >}}

First we build the docker image 
{{< highlight bash "linenos=table" >}}
docker build -f ./Dockerfile -t unsettled .
{{< / highlight >}}

If build goes well we will have an Hugo image build. To run the local dev 
{{< highlight bash "linenos=table" >}}
docker run -v $(pwd)/site:/site -v $(pwd)/public:/public --env-file ./.env --rm -p 1313:1313 -it unsettled
{{< / highlight >}}

The docker command to for just content compilation
{{< highlight bash "linenos=table" >}}
docker run --rm -v $(pwd)/site:/site -v $(pwd)/public:/public --env-file ./.env -p 1313:1313 unsettled compile
{{< / highlight >}}