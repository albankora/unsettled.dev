+++
author = "Alban"
title = "Bring the noise down and focus on what matters the most"
date = "2020-07-30"
description = "A post that talk's about focusing on things that matters with the least noise possible."
tags = [
    "teams",
]
categories = [
    "teams",
]
series = ["high performing teams"]
+++
Did you ever wonder why some teams appear to be more productive than others, is because they know how to focus on what brings the highest value with the least noise.
<!--more-->
#### `Getting things done in software development requires a lot of focus on things that matter with the least noise possible.`

## Less noise, faster progress
If you want your tickets to move faster from concept to production then you need to bring the noise down. What do we mean by bringing the noise down? Imagine that you have to build a feature that requires input from different teams `Content team` needs to provide some content, `Translation team` need to translate the content to different languages, `UI team` needs to build the designs, `Infrastructure team` need to do some infrastructure changes, `Development team` needs to update the front-end and backend and then `Testing team` need to test the feature. If we create a ticket for each team then the sequence of delivering those tickets will create a lot of noise with a lot of back and forth between those teams. We can split the future into two tickets than six and put a dependency between then. So we will end up with a design ticket that includes content and translation processes inside, and an implementation ticket that will take into consideration Front-end, backend, testing, and infrastructure changes. Tickets end up being grouped together in a way that keep the team more focused to deliver a fully functional ticket.

## Keep the focus on user stories.
There are different types of tickets the one you need to focus the most is the user stories. Because that’s what matters for the business or your stakeholders. I see teams focusing so much on technical tickets and losing focus on the user stories. As a product team, your focus should be on the user and their needs and how to get things done for them.

## Get the complex things done first.
When you start the development of a new product the focus must be on the unknown complex parts. Using a framework like `Cynefin` to categorize your user stories will help you keep focused on what is complex. Start doing the investigation required to clear up most of the unknown topics but keep in mind that if you focus to get all unknowns clarified you may lose a lot of time without showing any progress so finding the right balance is key to success.

## Mind the incremental development gap.
Doing incremental development is the way to go most of the times and using a process like `user story mapping` you can have a nice documented split on the version user stories will be available to the user. One thing to keep in mind when doing incremental development, and you are under pressure to deliver you `version 1` should be shippable. A lot of teams forget this, and they just hack something on top of the legacy system and call it `version 1`. The stakeholders are perfectly fine with version 1 and this is how the big bull of mud is built. 

## Be clear on the ticket category.
There are different types of tickets from user stories, technical tasks, and bug fixes. There is also a category that applies to those tickets. Those categories are `must-have tickets` and `good to have` tickets. You may have user stories, technical tasks, and bugs that belong to the must-have category also user stories, technical tasks and bugs that belong to the good to have. Knowing the difference between those two makes a huge difference when it comes to product delivery.

## The future may never come.
There is a mindset of coding for the future by adding a lot of abstraction upfront and that is useful sometimes but is a big waste most of the time. Finding the middle ground is not an easy job. Engineers with a good understanding of when abstraction is needed and when you are perfectly fine without it are important for the team. Also knowing what corners you are cutting when you have delivery deadlines and communicating those cuts successfully is an important skill to have.

## Recap
Above I describe several issues I have found working on various teams. Focusing on what matters and keeping the noise down is not an easy job is a constant effort put in on a daily basis by the team itself. 