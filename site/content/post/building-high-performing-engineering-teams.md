+++
author = "Alban"
title = "Building high performing engineering teams"
date = "2020-07-08"
description = "A post that talk's about build high performing engineering teams"
tags = [
    "teams",
]
categories = [
    "teams",
]
series = ["high performing teams"]
+++
Did you ever wonder how do you build high performing engineering teams?
<!--more-->

#### `Building high performing teams is all about people and the way they solve problems`

## Hiring
The first and most important factor on building high performing teams is to get the people with the right attitude in. Soft skills are more important than hard skills most of the time. Of course when you hire a Senior OR Lead engineers then hard skills are equally important to soft skills. 

## Retros
Retrospectives meetings are you team's health checks. Having regular meetings talking about what `went well` and what `needs to be improved` with clear action points and owners on getting those action points done is crucial for a team to improve. Also avoid having line managers on reto meetings as will prevent people from being open.

## Celebrate your successes
You need to build a `celebration culture` inside your team. When you team achieve curtain milestones then get a team activity paid by the company or just hit the pub an hour earlier than usual you deserve it. 

## Learn from your mistakes
There are not individual mistakes on a team but only team mistakes. Having healthy `post-mortem meetings` OR `topic specific retros` are crucial for learning.

## Get people involved
Try to involved people in different discussion for future products or discovery. By getting people involved will increase team engagement. Rule of thumb `Always do discovery meetings as a team`.

## Cross functionality build in
A high performing team should have most of the expertise required to deliver a product in the team itself. The more expertise a team has the less we will need to communicate with other teams to get things done.

## Get to know your cycle time
Cycle time describes how much time it takes for a user story to get from in-progress to production where actual users are using it. This will help the team improve their estimation for future products. It will make more visible when the slicing of a user story was not the right one. It will also help to identify underling problems that are not easily visible.   

## Recap
Building high performing engineering teams is a journey not a destination. A team can go to high performing to non-performing really fast. Making a team high performing will take some time and effort from everyone in the team. Having the right mixture of people and process will keep the team healthier for longer. What is described in this article is just the beginning. 