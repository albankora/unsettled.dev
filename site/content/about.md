+++
title = "About"
description = "Hugo, the world’s fastest framework for building websites"
date = "2020-06-24"
author = "Alban"
+++
# "I am blogging, so I can remember"
<!--more-->
Hi I am Alban a software engineer currently in London working for [Zava](https://zavamed.com). Having more than 10 years of professional experience. I have an opinion most of the time but also try to listen as I recognize my opinion may not be the best. I have worked in companies with 2 people and companies with 2000+ people on industries like Health, Film post-production, Fin-tech, Betting and Digital Agencies.

I enjoy programming in PHP, Javascript, Golang and a bit of Dart. I also enjoy working with AWS, Docker, Terraform, AWS CDK, microservices, event driven architectures and using [C4Model](https://c4model.com/) to document system architecture. Always looking for better ways to communicate with stake holders and build better software using DDD, BDD TDD, EventStorming, User Story Mapping and Example Mapping. 

If you have any questions OR want to ask me something please contact me through [Email](mailto:ankdeveloper@gmail.com) OR [Twitter](https://twitter.com/albankora) OR [Linkedin](https://www.linkedin.com/in/albankora)
