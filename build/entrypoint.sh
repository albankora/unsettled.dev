#!/bin/sh

set -ex

WATCH="${HUGO_WATCH:=false}"
SLEEP="${HUGO_REFRESH_TIME:=-1}"
HUGO_DEV_DESTINATION="${HUGO_DEV_DESTINATION:=/public}"
HUGO_PRD_DESTINATION="${HUGO_PRD_DESTINATION:=/published}"
echo "HUGO_WATCH:" $WATCH
echo "HUGO_REFRESH_TIME:" $HUGO_REFRESH_TIME
echo "HUGO_DEV_BASEURL" $HUGO_DEV_BASEURL
echo "HUGO_PRD_BASEURL" $HUGO_PRD_BASEURL
echo "ARGS" $@

HUGO=/usr/local/sbin/hugo
echo "Hugo path: $HUGO"

while [ -z "$@" ]
do
    if [[ $HUGO_WATCH != 'false' ]]; then
	    echo "Watching..."
        $HUGO server --watch=true --source="/site" --destination="$HUGO_DEV_DESTINATION" --baseURL="$HUGO_DEV_BASEURL" --bind="0.0.0.0" "$@" || exit 1
    fi

    if [[ $HUGO_REFRESH_TIME == -1 ]]; then
        exit 0
    fi
    echo "Sleeping for $HUGO_REFRESH_TIME seconds..."
    sleep $SLEEP
done

if [[ $@ == 'compile' ]]; then
    echo "compile theme"
    $HUGO --source="/site" --destination="$HUGO_PRD_DESTINATION" --baseURL="$HUGO_PRD_BASEURL" || exit 1
    exit 0
fi

exec $@